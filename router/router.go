package router

import (
	"fmt"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"

	"test-axiata-digital-labs/internal/handler"
	"test-axiata-digital-labs/internal/repository"
	"test-axiata-digital-labs/internal/usecase"
)

const REQUEST = "/api/posts/:id"

func NewRouter(db *sqlx.DB) *httprouter.Router {

	postRepo := repository.NewPostReposity(db)
	postUC := usecase.NewPostUseCase(5*time.Second, postRepo, db)
	postHandler := handler.NewPostHandler(postUC)

	userRepo := repository.NewUserRepository(db)
	userUC := usecase.NewUserUseCase(5*time.Second, userRepo, db)
	userHandler := handler.NewUserHandler(userUC)

	router := httprouter.New()

	router.GET("/", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		fmt.Fprintf(w, "Hello World!")
	})

	router.POST("/api/posts", postHandler.CreatePost)
	router.GET(REQUEST, postHandler.GetPosts)
	router.PUT(REQUEST, postHandler.UpdatePost)
	router.DELETE(REQUEST, postHandler.DeletePost)
	router.GET("/api/V2/posts/:tag", postHandler.GetByTag)

	router.POST("/api/register", userHandler.RegisterUser)
	router.POST("/api/login", userHandler.Login)

	return router
}
