package usecase

import (
	"context"
	"errors"
	"log"
	"test-axiata-digital-labs/internal/libs/middleware"
	"test-axiata-digital-labs/internal/model"
	"test-axiata-digital-labs/internal/repository"
	"time"

	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
)

type UserUseCase interface {
	RegisterUser(ctx context.Context, request *model.RegisterRequest) error
	Login(ctx context.Context, request *model.LoginParam) (*model.LoginResponse, error)
}

type userUseCase struct {
	contexTimeout  time.Duration
	userRepository repository.UserRepository
	db             *sqlx.DB
}

func NewUserUseCase(contextTimeout time.Duration, userRepository repository.UserRepository, db *sqlx.DB) UserUseCase {
	return &userUseCase{
		contextTimeout,
		userRepository,
		db,
	}
}

func (u *userUseCase) RegisterUser(ctx context.Context, request *model.RegisterRequest) error {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	checkUser, err := u.userRepository.GetUserByEmail(ctx, request.Email)
	if err != nil {
		log.Println("SQL error in UseCase RegisterUser => Open Transaction GetUserByEmail", err)
		return err
	}

	if checkUser {
		return errors.New("email already registered")
	}

	err = u.userRepository.RegisterUser(ctx, request)
	if err != nil {
		log.Println("SQL error in UseCase RegisterUser => Open Transaction RegisterUser", err)
		return err
	}

	return nil
}

func (u *userUseCase) Login(ctx context.Context, request *model.LoginParam) (*model.LoginResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	var result *model.LoginResponse

	user, err := u.userRepository.FindUserByEmail(ctx, request.Email)
	if err != nil {
		log.Println("SQL error in UseCase Login => Open Transaction FindUserByEmail", err)
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))
	if err != nil {
		log.Println("SQL error in UseCase Compare Password => Open Transaction FindUserByEmail", err)
		return nil, err
	}

	token, err := middleware.GenerateToken(user.Fullname, user.Email, user.Roles)
	if err != nil {
		log.Println("SQL error in UseCase Generate Token => Open Transaction FindUserByEmail", err)
		return nil, err
	}

	result = &model.LoginResponse{
		JwtToken: token,
	}

	return result, nil
}
