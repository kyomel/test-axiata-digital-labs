package usecase

import (
	"context"
	"log"
	"test-axiata-digital-labs/internal/model"
	"test-axiata-digital-labs/internal/repository"
	"time"

	"github.com/jmoiron/sqlx"
)

type PostUseCase interface {
	CreatePost(ctx context.Context, request *model.Posts) (*model.Posts, error)
	GetPosts(ctx context.Context, postID int) (*model.PostLists, error)
	UpdatePosts(ctx context.Context, postID int, request *model.UpdatePostRequest) (*model.Posts, error)
	DeletePost(ctx context.Context, postID int) error
	GetByTag(ctx context.Context, label string) ([]model.SearchPostResponse, error)
}

type postUseCase struct {
	contexTimeout  time.Duration
	postRepository repository.PostRepository
	db             *sqlx.DB
}

func NewPostUseCase(contextTimeout time.Duration, postRepository repository.PostRepository, db *sqlx.DB) PostUseCase {
	return &postUseCase{
		contextTimeout,
		postRepository,
		db,
	}
}

func (u *postUseCase) CreatePost(ctx context.Context, request *model.Posts) (*model.Posts, error) {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	var result model.Posts

	post, err := u.postRepository.CreatePost(ctx, request)
	if err != nil {
		log.Println("SQL error in UseCase CreatePost => Open Transaction CreatePost", err)
		return nil, err
	}

	tag, err := u.postRepository.GetTag(ctx, request.Tags)
	if err != nil {
		log.Println("SQL error in UseCase CreatePost => Open Transaction GetTag", err)
		return nil, err
	}

	err = u.postRepository.InsertPostTag(ctx, post.ID, tag)
	if err != nil {
		log.Println("SQL error in UseCase InsertPostTag => Open Transaction InsertPostTag", err)
		return nil, err
	}

	result = model.Posts{
		Title:   post.Title,
		Content: post.Content,
		Tags:    request.Tags,
	}

	return &result, nil
}

func (u *postUseCase) GetPosts(ctx context.Context, postID int) (*model.PostLists, error) {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	post, err := u.postRepository.GetPost(ctx, postID)
	if err != nil {
		log.Println("SQL error in UseCase GetPosts => Open Transaction GetPost", err)
		return nil, err
	}

	tag, err := u.postRepository.GetTags(ctx, postID)
	if err != nil {
		log.Println("SQL error in UseCase GetPosts => Open Transaction GetTags", err)
		return nil, err
	}

	result := model.PostLists{
		Title:       post.Title,
		Content:     post.Content,
		Status:      post.Status,
		PublishDate: post.PublishDate,
		Tags:        tag,
	}

	return &result, nil
}

func (u *postUseCase) UpdatePosts(ctx context.Context, postID int, request *model.UpdatePostRequest) (*model.Posts, error) {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	var result model.Posts

	updatePost, err := u.postRepository.UpdatePost(ctx, postID, request)
	if err != nil {
		log.Println("SQL error in UseCase UpdatePosts => Open Transaction UpdatePost", err)
		return nil, err
	}

	tags, err := u.postRepository.GetTag(ctx, request.Tags)
	if err != nil {
		log.Println("SQL error in UseCase UpdatePosts => Open Transaction GetTag", err)
		return nil, err
	}

	err = u.postRepository.InsertPostTag(ctx, updatePost.ID, tags)
	if err != nil {
		log.Println("SQL error in UseCase UpdatePosts => Open Transaction InsertPostTag", err)
		return nil, err
	}

	result = model.Posts{
		Title:   updatePost.Title,
		Content: updatePost.Content,
		Tags:    request.Tags,
	}

	return &result, nil

}

func (u *postUseCase) DeletePost(ctx context.Context, postID int) error {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	err := u.postRepository.DeletePost(ctx, postID)
	if err != nil {
		log.Println("SQL error in UseCase DeletePost => Open Transaction DeletePost", err)
		return err
	}

	return err
}

func (u *postUseCase) GetByTag(ctx context.Context, label string) ([]model.SearchPostResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, u.contexTimeout)
	defer cancel()

	searchPost, err := u.postRepository.GetByTag(ctx, label)
	if err != nil {
		log.Println("SQL error in UseCase GetByTag => Open Transaction GetByTag", err)
		return nil, err
	}

	return searchPost, nil
}
