package model

type RegisterRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Roles    string `json:"roles"`
	Fullname string `json:"fullname"`
}

type RegisterResponse struct {
	ID       int    `json:"id"`
	Fullname string `json:"fullname"`
	Email    string `json:"email"`
	Password string `json:"-"`
}

type LoginParam struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserSession struct {
	JWTToken string `json:"jwt_token"`
}

type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"-"`
}

type UserLogin struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Fullname string `json:"fullname"`
	Roles    string `json:"roles"`
}

type LoginResponse struct {
	JwtToken string `json:"jwt_token"`
}
