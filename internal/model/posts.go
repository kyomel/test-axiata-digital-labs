package model

import "time"

type PostRepo struct {
	ID      int    `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Tags    []Tag  `json:"tags"`
}

type Tag struct {
	ID    int    `json:"id"`
	Label string `json:"label"`
}

type TagResponse struct {
	Label string `json:"label"`
}

type Posts struct {
	Title   string   `json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
}

type PostLists struct {
	Title       string        `json:"title"`
	Content     string        `json:"content"`
	Status      string        `json:"status"`
	PublishDate time.Time     `json:"publish_date"`
	Tags        []TagResponse `json:"tags"`
}

type UpdatePostRequest struct {
	Title   string   ` json:"title"`
	Content string   `json:"content"`
	Tags    []string `json:"tags"`
}

type SearchPostResponse struct {
	ID          int      `json:"id"`
	Title       string   `json:"title"`
	Content     string   `json:"content"`
	Tags        []string `json:"tags"`
	Status      string   `json:"status"`
	PublishDate string   `json:"publish_date"`
}
