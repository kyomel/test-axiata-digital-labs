package repository

import (
	"context"
	"log"
	"test-axiata-digital-labs/internal/libs/helper"
	"test-axiata-digital-labs/internal/model"

	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
)

type UserRepository interface {
	RegisterUser(ctx context.Context, req *model.RegisterRequest) error
	GetUserByEmail(ctx context.Context, userEmail string) (bool, error)
	FindUserByEmail(ctx context.Context, email string) (*model.UserLogin, error)
}

type userRepo struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) UserRepository {
	return &userRepo{
		db,
	}
}

func (u *userRepo) RegisterUser(ctx context.Context, req *model.RegisterRequest) error {
	tx, err := u.db.Begin()
	if err != nil {
		return err
	}

	hashPasword, err := getHashPassword(req.Password)
	if err != nil {
		log.Println("Error on Hash Password => ", err)
		return err
	}

	query := `
		INSERT INTO users(email, password, fullname, roles)
		VALUES($1, $2, $3, $4)
	`

	_, err = tx.QueryContext(ctx, query, req.Email, hashPasword, req.Fullname, req.Roles)
	if err != nil {
		log.Println("SQL error on RegisterUser => Execute Query", err)
		return err
	}

	helper.CommitOrRollback(tx, err)

	return err
}

func getHashPassword(password string) (string, error) {
	bytePassword := []byte(password)
	hash, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

func (u *userRepo) GetUserByEmail(ctx context.Context, userEmail string) (bool, error) {
	var result bool

	query := `
		SELECT EXISTS (SELECT 1 FROM users WHERE email = $1)
	`

	row := u.db.QueryRowContext(ctx, query, userEmail)
	err := row.Scan(&result)
	if err != nil {
		log.Println("SQL error on GetUserByEmail => Execute Query and Scan", err)
		return false, err
	}

	return result, nil
}

func (u *userRepo) FindUserByEmail(ctx context.Context, email string) (*model.UserLogin, error) {
	var result model.UserLogin

	query := `
		SELECT id, email, password, fullname, roles
		FROM users
		WHERE email = $1
	`

	row := u.db.QueryRowContext(ctx, query, email)
	err := row.Scan(
		&result.ID,
		&result.Email,
		&result.Password,
		&result.Fullname,
		&result.Roles,
	)
	if err != nil {
		log.Println("SQL error on FindUserByEmail => Execute Query and Scan", err)
		return nil, err
	}

	return &result, nil
}
