package repository

import (
	"context"
	"fmt"
	"log"
	"test-axiata-digital-labs/internal/libs/helper"
	"test-axiata-digital-labs/internal/model"

	"github.com/jmoiron/sqlx"
)

type PostRepository interface {
	CreatePost(ctx context.Context, request *model.Posts) (*model.PostRepo, error)
	GetTag(ctx context.Context, request []string) ([]model.Tag, error)
	InsertPostTag(ctx context.Context, postID int, tags []model.Tag) error
	GetPost(ctx context.Context, postID int) (*model.PostLists, error)
	GetTags(ctx context.Context, postID int) ([]model.TagResponse, error)
	UpdatePost(ctx context.Context, postID int, request *model.UpdatePostRequest) (*model.PostRepo, error)
	DeletePost(ctx context.Context, postID int) error
	GetByTag(ctx context.Context, label string) ([]model.SearchPostResponse, error)
}

type postRepository struct {
	db *sqlx.DB
}

func NewPostReposity(db *sqlx.DB) PostRepository {
	return &postRepository{
		db,
	}
}

func (r *postRepository) CreatePost(ctx context.Context, request *model.Posts) (*model.PostRepo, error) {
	var result model.PostRepo

	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	query := `
		INSERT INTO posts(title, content, status, publish_date)
		VALUES($1, $2, 'published', current_timestamp)
		RETURNING id, title, content
	`

	row := tx.QueryRowContext(ctx, query, request.Title, request.Content)
	err = row.Scan(&result.ID, &result.Title, &result.Content)
	if err != nil {
		log.Println("SQL error on CreatePost => Execute Query and Scan", err)
		return nil, err
	}

	helper.CommitOrRollback(tx, err)

	return &result, nil
}

func (r *postRepository) GetTag(ctx context.Context, request []string) ([]model.Tag, error) {
	var (
		result []model.Tag
		values string
	)

	for _, v := range request {
		values += fmt.Sprintf(`'%s',`, v)
	}
	values = values[:len(values)-1]

	sqlQuery := fmt.Sprintf(`
		SELECT
			id, label
		FROM tags
		WHERE label IN (%s)
	`, values)

	rows, err := r.db.QueryContext(ctx, sqlQuery)
	if err != nil {
		log.Println("SQL error on GetTag => Execute Query", err)
		return result, err
	} else {
		defer rows.Close()
		for rows.Next() {
			var i model.Tag
			err := rows.Scan(
				&i.ID,
				&i.Label,
			)

			if err != nil {
				log.Println("[err] [repository] [Scan] => ", err)
			} else {
				result = append(result, i)
			}
		}

		if err := rows.Close(); err != nil {
			return result, err
		}

		if err := rows.Err(); err != nil {
			return result, err
		}
	}

	return result, nil
}

func (r *postRepository) InsertPostTag(ctx context.Context, postID int, tags []model.Tag) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec("DELETE FROM post_tags WHERE post_id = $1", postID)
	if err != nil {
		log.Println("SQL error on InsertPostTag => Execute Query", err)
		return err
	}

	for _, tag := range tags {
		_, err := tx.Exec("INSERT INTO post_tags(post_id, tag_id) VALUES ($1, $2)", postID, tag.ID)
		if err != nil {
			log.Println("SQL error on InsertPostTag => Execute Query", err)
			return err
		}
	}

	helper.CommitOrRollback(tx, err)

	return err
}

func (r *postRepository) GetPost(ctx context.Context, postID int) (*model.PostLists, error) {
	var result model.PostLists

	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	query := `
		SELECT 
			p.title, p.content, p.status, p.publish_date
		FROM posts p
		where p.id = $1
	`

	rows := tx.QueryRowContext(ctx, query, postID)
	err = rows.Scan(
		&result.Title,
		&result.Content,
		&result.Status,
		&result.PublishDate,
	)

	if err != nil {
		log.Println("SQL error on GetPost => Execute Query and Scan", err)
		return nil, err
	}

	helper.CommitOrRollback(tx, err)

	return &result, nil
}

func (r *postRepository) GetTags(ctx context.Context, postID int) ([]model.TagResponse, error) {
	var result []model.TagResponse

	query := `
		SELECT 
			t."label" 
		FROM post_tags pt 
		LEFT JOIN tags t on t.id = pt.tag_id 
		WHERE pt.post_id = $1
	`

	rows, err := r.db.QueryContext(ctx, query, postID)
	if err != nil {
		log.Println("SQL error on GetTags => Execute Query", err)
		return result, err
	} else {
		defer rows.Close()
		for rows.Next() {
			var i model.TagResponse
			err := rows.Scan(
				&i.Label,
			)

			if err != nil {
				log.Println("[err] [repository] [Scan] => ", err)
			} else {
				result = append(result, i)
			}
		}

		if err := rows.Close(); err != nil {
			return result, err
		}

		if err := rows.Close(); err != nil {
			return result, err
		}
	}

	return result, nil
}

func (r *postRepository) UpdatePost(ctx context.Context, postID int, request *model.UpdatePostRequest) (*model.PostRepo, error) {
	var result model.PostRepo

	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	query := `
		update posts 
		set 
			title = $2,
			"content" = $3
		where id = $1
		RETURNING id, title, "content" 
	`

	row := tx.QueryRowContext(ctx, query, postID, request.Title, request.Content)
	err = row.Scan(&result.ID, &result.Title, &result.Content)
	if err != nil {
		log.Println("SQL error on UpdatePost => Execute Query and Scan", err)
		return nil, err
	}

	helper.CommitOrRollback(tx, err)

	return &result, nil
}

func (r *postRepository) DeletePost(ctx context.Context, postID int) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec("DELETE FROM post_tags WHERE post_id = $1", postID)
	if err != nil {
		log.Println("SQL error on DeletePost => Execute Query", err)
		return err
	}

	_, err = tx.Exec("DELETE FROM posts WHERE id = $1", postID)
	if err != nil {
		log.Println("SQL error on DeletePost => Execute Query", err)
		return err
	}

	helper.CommitOrRollback(tx, err)

	return err
}

func (r *postRepository) GetByTag(ctx context.Context, label string) ([]model.SearchPostResponse, error) {
	rows, err := r.db.Query(`
        SELECT p.id, p.title, p.content, p.status, p.publish_date
        FROM posts p
        JOIN post_tags pt ON p.id = pt.post_id
        JOIN tags t ON pt.tag_id = t.id
        WHERE t.label = $1`, label)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var posts []model.SearchPostResponse
	for rows.Next() {
		var p model.SearchPostResponse
		if err := rows.Scan(&p.ID, &p.Title, &p.Content, &p.Status, &p.PublishDate); err != nil {
			return nil, err
		}

		tagRows, err := r.db.Query(`
            SELECT t.label
            FROM tags t
            JOIN post_tags pt ON t.id = pt.tag_id
            WHERE pt.post_id = $1`, p.ID)
		if err != nil {
			return nil, err
		}
		defer tagRows.Close()

		var tags []string
		for tagRows.Next() {
			var tag string
			if err := tagRows.Scan(&tag); err != nil {
				return nil, err
			}
			tags = append(tags, tag)
		}
		p.Tags = tags

		posts = append(posts, p)
	}
	return posts, nil
}
