package utils

const (
	INTERNAL_SERVER_ERROR        = "internal server error"
	NOT_FOUND_ERROR              = "data not found"
	BAD_REQUEST                  = "bad request"
	STATUS_INTERNAL_SERVER_ERROR = 500
)
