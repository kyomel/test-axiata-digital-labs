package middleware

import (
	"time"

	"os"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	Fullname string `json:"fullname"`
	Email    string `json:"email"`
	Roles    string `json:"roles"`
	jwt.StandardClaims
}

func GenerateToken(fullname, email, roles string) (string, error) {
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		Fullname: fullname,
		Email:    email,
		Roles:    roles,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
