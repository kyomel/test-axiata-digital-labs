package handler

import (
	"net/http"
	"strconv"
	"test-axiata-digital-labs/internal/libs/helper"
	"test-axiata-digital-labs/internal/model"
	"test-axiata-digital-labs/internal/usecase"

	"github.com/julienschmidt/httprouter"
)

type PostHandler interface {
	CreatePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
	GetPosts(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
	UpdatePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
	DeletePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
	GetByTag(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
}

type postHandler struct {
	postUC usecase.PostUseCase
}

func NewPostHandler(postUC usecase.PostUseCase) PostHandler {
	return &postHandler{
		postUC,
	}
}

func (h *postHandler) CreatePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	var request model.Posts
	helper.ReadRequestBody(requests, &request)

	res, err := h.postUC.CreatePost(requests.Context(), &request)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   res,
	}

	helper.WriteResponseBody(writer, webResponse)
}

func (h *postHandler) GetPosts(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	postID := params.ByName("id")
	id, _ := strconv.Atoi(postID)

	posts, err := h.postUC.GetPosts(requests.Context(), id)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   posts,
	}

	helper.WriteResponseBody(writer, webResponse)
}

func (h *postHandler) UpdatePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	var request model.UpdatePostRequest
	helper.ReadRequestBody(requests, &request)

	postID := params.ByName("id")
	id, _ := strconv.Atoi(postID)

	posts, err := h.postUC.UpdatePosts(requests.Context(), id, &request)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   posts,
	}

	helper.WriteResponseBody(writer, webResponse)
}

func (h *postHandler) DeletePost(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	postID := params.ByName("id")
	id, _ := strconv.Atoi(postID)

	err := h.postUC.DeletePost(requests.Context(), id)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
	}

	helper.WriteResponseBody(writer, webResponse)
}

func (h *postHandler) GetByTag(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	tags := params.ByName("tag")

	searchPost, err := h.postUC.GetByTag(requests.Context(), tags)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   searchPost,
	}

	helper.WriteResponseBody(writer, webResponse)
}
