package handler

import (
	"net/http"
	"test-axiata-digital-labs/internal/libs/helper"
	"test-axiata-digital-labs/internal/model"
	"test-axiata-digital-labs/internal/usecase"

	"github.com/julienschmidt/httprouter"
)

type UserHandler interface {
	RegisterUser(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
	Login(writer http.ResponseWriter, requests *http.Request, params httprouter.Params)
}

type userHandler struct {
	userUC usecase.UserUseCase
}

func NewUserHandler(userUC usecase.UserUseCase) UserHandler {
	return &userHandler{
		userUC,
	}
}

func (h *userHandler) RegisterUser(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	var request model.RegisterRequest

	helper.ReadRequestBody(requests, &request)

	err := h.userUC.RegisterUser(requests.Context(), &request)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   nil,
	}

	helper.WriteResponseBody(writer, webResponse)
}

func (h *userHandler) Login(writer http.ResponseWriter, requests *http.Request, params httprouter.Params) {
	var request model.LoginParam

	helper.ReadRequestBody(requests, &request)

	token, err := h.userUC.Login(requests.Context(), &request)
	if err != nil {
		helper.PanicIfError(err)
	}

	webResponse := model.WebResponse{
		Code:   200,
		Status: "Ok",
		Data:   token,
	}

	helper.WriteResponseBody(writer, webResponse)
}
