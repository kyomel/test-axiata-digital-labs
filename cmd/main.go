package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	databases "test-axiata-digital-labs/db"
	"test-axiata-digital-labs/router"

	"github.com/joho/godotenv"
)

var (
	dbRepoConn databases.DatabaseRepo = databases.NewPostgresRepo()
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file", err)
	}

	port := os.Getenv("PORT")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	sslMode := os.Getenv("SSL_MODE")

	portDB, _ := strconv.Atoi(dbPort)
	portConnect, _ := strconv.Atoi(port)

	db, err := dbRepoConn.Connect(dbHost, portDB, dbUser, dbPassword, dbName, sslMode)
	if err != nil {
		log.Fatal(err)
	}

	r := router.NewRouter(db)
	log.Println("Listening on port", portConnect)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", portConnect), r))
}
