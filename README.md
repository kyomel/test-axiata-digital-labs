# Test Backend Axiata Digital Labs
 This is a test project for Axia Digital Labs.

## Installation
1. Clone the repository:
```
https://gitlab.com/kyomel/test-axiata-digital-labs.git
```
2. Change into the project directory:
```
cd test-axiata-digital-labs
```
3. Copy the .env.example file to .env and update the environment variables:
```
cp .env.example .env
```
4. Install the dependencies:
```
go mod download
```
5. Install the dependencies:
```
createdb test-axiata-digital-labs
```
6. Run the migrations:
```
migrate -path db/migration -database "postgresql://postgres:{password}@localhost:5432/test-axiata-digital-labs?sslmode=disable" -verbose up
```
```
migrate -path db/migration -database "postgresql://postgres:{password}@localhost:5432/test-axiata-digital-labs?sslmode=disable" -verbose down
```
7. Run the application:
```
go run cmd/main.go
```
8. Check at POSTMAN:
```
POST-{url}/api/posts
GET-{url}/api/posts/:id
PUT-{url}/api/posts/:id
DELETE-{url}/api/posts/:id
GET-{url}/api/V2/posts/:tag
```