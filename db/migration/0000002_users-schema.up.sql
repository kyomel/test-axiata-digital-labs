CREATE TABLE IF NOT EXISTS users (
	id serial primary key,
	email VARCHAR(150) not null,
	password VARCHAR(150) not null, 
	fullname VARCHAR(150) not null
);

ALTER TABLE users ADD COLUMN IF NOT EXISTS roles varchar(50);